function stolenLunch(note) {
  var result = ""
  for (var i = 0; i < note.length; i++) {
    var text = note[i]
    if (/[0-9]/.test(note[i]))
      text = toChar(note[i])
    if (/[a-j]/.test(note[i]))
      text = toNumber(note[i])
    result = result.concat(text)
  }
  return result
}

function toChar(n) {
  return String.fromCharCode(97 + Number(n))
}

function toNumber(s) {
  return s.charCodeAt(0) - 97
}
