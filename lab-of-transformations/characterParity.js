function characterParity(symbol) {
  return (/[0-9]/.test(symbol)) ? (((symbol & 1) == 0) ? "even" : "odd") : "not a digit"
}
