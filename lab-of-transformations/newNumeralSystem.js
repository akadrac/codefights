function newNumeralSystem(number) {
  var value = toDecimal(number)
  var result = []
  for (var i = 0; i < (value + 1) / 2; i++) {
    result.push(toChar(i) + " + " + toChar(value - i))
  }

  return result
}

function toDecimal(s) {
  return s.charCodeAt(0) - 65
}

function toChar(n) {
  return String.fromCharCode(n + 65)
}
