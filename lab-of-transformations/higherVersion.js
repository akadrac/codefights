function higherVersion(ver1, ver2) {
  return breakIt(ver1) > breakIt(ver2)
}

function breakIt(version) {
  var v = version.split(".")
  var calc = 0
  var mod = 1
  for (var i = v.length - 1; i >= 0; i--) {
    calc += v[i] * mod
    mod *= 100
  }
  console.log(calc)
  return calc
}
