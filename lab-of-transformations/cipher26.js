function cipher26(message) {
  var calc = toDecimal(message[0])
  var result = message[0]
  for (var i = 1; i < message.length; i++) {
    var value = toDecimal(message[i])

    var c = (value - calc + 26) % 26
    calc = (calc + c) % 26

    result = result.concat(toChar(c))
  }
  return result
}

function toDecimal(s) {
  return s.charCodeAt(0) - 97
}

function toChar(n) {
  return String.fromCharCode(n + 97)
}
