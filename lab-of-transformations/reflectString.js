function reflectString(inputString) {
  var result = ""
  for (var i = 0; i < inputString.length; i++) {
    result = result.concat(flip(inputString[i]))
  }
  return result
}

function flip(char) {
  var pos = char.charCodeAt(0) - 97
  var inv = 97 + 25 - pos
  console.log("%s,%s", pos, inv)
  return String.fromCharCode(inv)
}
