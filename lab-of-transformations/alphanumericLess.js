function alphanumericLess(s1, s2) {
  t1 = split(s1)
  console.log(t1)
  t2 = split(s2)
  console.log(t2)

  for (var i = 0; i < t1.length; i++) {
    if (i > t1.length)
      return false

    var a = t1[i]
    var b = t2[i]

    if (!isNaN(a)) {
      if (!isNaN(b)) {
        if (a != b)
          return a < b
      }
      else
        return true
    }
    else
      if (a != b)
        return a < b

  }

  if (t1.length != t2.length) {
    return t1.length < t2.length
  }

  return s1.length > s2.length
}

function split(s) {
  var i = 0
  var token = []
  do {
    if (/[0-9]/.test(s[i])) {
      var value = ""
      do {
        value += s[i]
        i++
      } while (/[0-9]/.test(s[i]))
      token.push(parseInt(value))
    }
    else {
      token.push(s[i])
      i++
    }
  } while (s.length > i)
  return token
}
