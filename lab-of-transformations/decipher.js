function decipher(cipher) {
  var i = 0
  var result = ""
  var value = 0
  do {
    if (cipher[i] == 1) {
      value = cipher.substring(i, i + 3)
      i += 3
    }
    else {
      value = cipher.substring(i, i + 2)
      i += 2
    }
    result = result.concat(String.fromCharCode(value))
  } while (cipher.length > i)
  return result
}
