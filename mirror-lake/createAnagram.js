function createAnagram(s, t) {
  var a = uniqueLetters(s.split(""))
  console.log(a)
  var b = uniqueLetters(t.split(""))
  console.log(b)
  var count = 0;
  for (var i = 0; i < 26; i++) {
    if (a[i] == b[i])
      continue

    if (a[i] > b[i])
      count += a[i] - b[i]
    else
      count += b[i] - a[i]
  }
  return count / 2
}

function uniqueLetters(arrayOfChars) {
  var alpha = new Array(26)
  alpha.fill(0)
  for (var i = 0; i < arrayOfChars.length; i++) {
    alpha[arrayOfChars[i].charCodeAt(0) - 65]++
  }
  // return alpha.filter( (elm, index) => {return elm })
  return alpha
}
