function isSubstitutionCipher(string1, string2) {
  return uniqueLetters(string1.split("")) == uniqueLetters(string2.split(""))
}

function uniqueLetters(arrayOfChars) {
  var alpha = new Array(26)
  alpha.fill(0)
  for (var i = 0; i < arrayOfChars.length; i++) {
    alpha[arrayOfChars[i].charCodeAt(0) - 97]++ // will result in a = 0, z = 26
  }
  return alpha.filter(elm => { return elm }).length // return the number of unique letters
}
