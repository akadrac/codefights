function numbersGrouping(a) {
  var array = new Array(100001)
  array.fill(0)
  for (var i = 0; i < a.length; i++) {
    array[Math.floor((a[i] - 1) / 10000)]++
  }

  var result = array.filter(elm => { return elm })

  return (result.length) ? result.reduce((p, c) => { return p + c }) + result.length : 0
}
