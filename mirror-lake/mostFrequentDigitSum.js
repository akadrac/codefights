function mostFrequentDigitSum(n) {
  let elements = []
  do {
    let s = sum(n)
    n -= s
    elements.push(s)
  } while (n > 0)
  let result = new Array(20)
  result.fill(0)
  for (i = 0; i < elements.length; i++) {
    result[elements[i]]++
  }
  // console.log(result)
  var hValue = 0
  var hIndex = 0
  result.find((element, index) => {
    if (element >= hValue && index > hIndex) {
      // console.log('a', element, index)
      hValue = element
      hIndex = index
    }
  })
  return hIndex
}

function sum(n) {
  return factorBy(n, 10).reduce((sum, value) => sum + value, 0)
}

function factorBy(n, factor = 10) {
  let result = []
  do {
    a = n % factor;
    n = b = (n - a) / factor;
    result.push(a)
  } while (b > 0)
  console.log(result)
  return result
}
