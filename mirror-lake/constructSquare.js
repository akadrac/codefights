function constructSquare(s) {
  var max = Math.pow(10, s.length) - 1

  var chars = uniqueChars(s)

  var answer = -1
  var prev = -1

  for (var i = 4, sq = 0; sq < max; i++) {
    sq = Math.pow(i, 2)
    var digits = uniqueDigits(sq)
    if (digits.length == chars.length) {
      for (var x = 0; x < chars.length; x++) {
        answer = sq // set the answer to the square
        if (chars[x] != digits[x]) {
          answer = prev // the number of unique digits and chars don't match.
          break
        }
      }
    }
    prev = answer
  }

  return answer
}

function uniqueChars(s) {
  var array = new Array(26)
  array.fill(0)

  for (var i = 0; i < s.length; i++) {
    array[toNumber(s[i])]++
  }
  return array.filter(elm => { return elm }).sort((a, b) => { return b - a })
}

function uniqueDigits(d) {
  var array = new Array(10)
  array.fill(0)
  var c = d.toString()

  for (var i = 0; i < c.length; i++) {
    array[c[i]]++
  }
  return array.filter(elm => { return elm }).sort((a, b) => { return b - a })
}

function toNumber(v) {
  return v.charCodeAt(v) - 97
}
