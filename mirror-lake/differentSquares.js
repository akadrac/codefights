function differentSquares(matrix) {
  var l = matrix.length - 1
  var w = matrix[0].length - 1
  if (l < 1 || w < 1)
    return 0

  let array = []
  for (i = 0; i < l; i++) {
    for (j = 0; j < w; j++) {
      let element = matrix[i][j] * 1000 + matrix[i][j + 1] * 100 + matrix[i + 1][j] * 10 + matrix[i + 1][j + 1]
      // console.log('a', element)
      if (array.lastIndexOf(element) == -1) {
        array.push(element)
      }
      // console.log('b', array)
    }
  }
  return array.length
}
