function largestNumber(n) {
  var d = 1;

  for (var i = 0; i < n; i++) {
    d = d * 10;
  }
  return d - 1;
}
