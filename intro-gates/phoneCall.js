function phoneCall(min1, min2_10, min11, S) {
  var charge = min1
  if (S - charge < 0) return 0;
  if (S - charge == 0) return 1;
  for (i = 2; i < 11; i++) {
    charge += min2_10;
    if (S - charge <= 0) return i - 1;
  }
  var extra = 0
  while (S - charge >= 0) {
    charge += min11;
    extra++;
  }
  return 9 + extra;
}
