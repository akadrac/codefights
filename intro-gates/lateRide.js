function lateRide(n) {
  function hours() {
    return (n - n % 60) / 60;
  }
  function minutes() {
    return (n % 60)
  }
  function addIntegars(n) {
    return n % 10 + (n - n % 10) / 10;
  }
  console.log(hours());
  console.log(minutes());
  return addIntegars(hours()) + addIntegars(minutes())
}
