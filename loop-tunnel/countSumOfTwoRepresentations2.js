function countSumOfTwoRepresentations2(n, l, r) {
  var i = 0;
  if (l + r != n && n - r > l)
    l = n - r;
  if (l + r != n && n - l < r)
    r = n - l;
  while (l <= r) {
    if (l + r == n)
      i++;
    l++;
    r--;
  }
  return i;
}
