function lineUp(commands) {
  var str = commands[Symbol.iterator]();
  var char;
  var pos = 0;
  var times = 0;
  while (char = str.next().value) {
    if (char == 'L')
      pos++;
    if (char == 'R')
      pos--;
    if (pos % 2 == 0)
      times++;

    console.log(char);
  }
  return times;
}
