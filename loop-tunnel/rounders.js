function rounders(value) {
  var scale = 10;
  while (scale <= value) {
    var rem = value % scale;
    value -= rem;
    if (rem >= scale / 2)
      value += scale;
    scale *= 10;
  }
  return value;
}
