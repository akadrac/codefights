function leastFactorial(n) {
  var factor = 1;
  var i = 2;
  while (n > factor) {
    factor *= i;
    i++;
  }
  return factor;
}
