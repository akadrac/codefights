function appleBoxes(k) {
  var b = 0;
  var y = 0;
  for (var i = 1; i <= k; i++) {
    if (i % 2 == 1)
      y += i * i;
    else
      b += i * i;
  }
  return b - y;
}
