function candles(candles, makeNew) {
  var burnt = candles;
  var left = candles;

  while (left >= makeNew) {
    candles = (left - (left % makeNew)) / makeNew;
    left += candles - (left - (left % makeNew));
    burnt += candles;
  }
  return burnt;
}
