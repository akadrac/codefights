function increaseNumberRoundness(n) {
  var str = n.toString()[Symbol.iterator]();
  var pin = false;
  while (char = str.next().value) {
    if (char !== '0' && pin)
      return true;
    if (char === '0' && !pin)
      pin = true;

  }
  return false;
}
