function countBlackCells(n, m) {
  if (n > m) {
    var t = n;
    n = m;
    m = t;
  }

  var gcd = ((a, b) => {
    if (a == 0)
      return b;

    while (b != 0) {
      if (a > b)
        a = a - b;
      else
        b = b - a;
    }

    return a;
  })(n, m)

  return n + m - gcd + (gcd - 1) * 2;
}
