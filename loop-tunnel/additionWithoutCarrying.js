function additionWithoutCarrying(param1, param2) {
  var result = 0;
  var scale = 1;
  while (param1 || param2) {
    var p1 = param1 % 10;
    param1 = (param1 - p1) / 10;
    var p2 = param2 % 10;
    param2 = (param2 - p2) / 10;
    var tmp = (p1 + p2) % 10;
    result += tmp * scale;
    scale *= 10;
  }
  return result;
}
