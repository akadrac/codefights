function mirrorBits(a) {
  var str = a.toString(2);
  var result = "";
  for (var i = str.length - 1; i >= 0; i--) {
    result = result.concat(str[i]);
  }
  return parseInt(result, 2);
}