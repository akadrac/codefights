function secondRightmostZeroBit(n) {
  return  ~n & ( (n | (n + 1)) + 1) ;
}
