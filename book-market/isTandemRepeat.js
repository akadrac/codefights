function isTandemRepeat(inputString) {
  var len = inputString.length
  return ((len & 1) == 0) ? (inputString.slice(0, Math.floor(len / 2)) === inputString.slice(Math.ceil(len / 2))) : false;
}
