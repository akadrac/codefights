function timedReading(maxLength, text) {
  var words = text.replace(/[^a-z\s]/gi, "").split(" ")
  var count = 0
  for (var i = 0; i < words.length; i++) {
    if (words[i].length <= maxLength && words[i].length != 0)
      count++
  }
  return count
}
