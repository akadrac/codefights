function allLongestStrings(inputArray) {
  if (inputArray.length == 1)
    return inputArray;

  var len = inputArray.reduce((prev, curr, index) => {
    if (index == 1)
      prev = prev.length

    if (curr.length > prev)
      return curr.length
    else
      return prev
  })
  return inputArray.filter(elm => { return elm.length == len })
}
