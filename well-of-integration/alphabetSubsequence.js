function alphabetSubsequence(s) {
  var prev = s[0] //assign first char
  for (var i = 1; i < s.length; i++) {
    if (prev >= s[i])
      return false
    prev = s[i]
  }
  return true
}
