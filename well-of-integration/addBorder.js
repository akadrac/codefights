function addBorder(picture) {
  var len = 0
  for (var i = 0; i < picture.length; i++) {
    if (picture[i].length > len) len = picture[i].length
  }
  len += 2 // extra length for sides

  var output = []
  for (var i = 0; i < picture.length + 2; i++) {
    var text = ""
    if (i == 0 || i == picture.length + 1)
      text = vertical(len, "")
    else
      text = horizontal(picture[i - 1])

    output.push(text)
  }
  return output
}

function vertical(l, t) {
  return (l <= 0) ? t : vertical(--l, t += "*")
}

function horizontal(t) {
  return "*" + t + "*"
}


