function areSimilar(A, B) {
  var count = 0
  var a1 = a2 = b1 = b2 = null
  for (var i = 0; i < A.length; i++) {
    if (A[i] != B[i]) {
      count++
      if (a1 == null) {
        a1 = A[i]
        b1 = B[i]
      } else {
        a2 = B[i]
        b2 = A[i]
      }
    }
  }
  return count == 0 || (count == 2 && (a1 == a2 && b1 == b2))
}

function old(A, B) {
  if (A.length !== B.length)
    return false
  if (!Array.isArray(A) || !Array.isArray(B))
    return false

  for (var i = 0; i < A.length; i++) {
    if (A[i] !== B[i]) {
      for (var x = i + 1; x < A.length; x++) {
        if (A[i] === B[x]) {
          var eq = B[x]
          var move = B[i]
          B.splice(i, 1, eq)
          B.splice(x, 1, move)
          break
        }
      }
      break
    }
  }
  return (A.equals(B)) ? true : false;
}

Array.prototype.equals = function (array) {
  if (this.length !== array.length)
    return false

  if (!Array.isArray(this) || !Array.isArray(array))
    return false

  for (var i = 0; i < this.length; i++) {
    if (this[i] !== array[i])
      return false
  }
  return true
}