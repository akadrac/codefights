function electionsWinners(votes, k) {
  votes.sort((a, b) => { return b - a })
  var top = votes[0]

  var possible = 0

  for (var i = 0; i < votes.length; i++) {
    if ((k == 0 && votes[i] == top) || (votes[i] + k > top)) {
      possible++
    }
  }

  return (k == 0 && possible > 1) ? 0 : possible
}
