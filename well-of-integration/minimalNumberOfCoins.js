function minimalNumberOfCoins(coins, price) {
  var count = 0
  for (var i = coins.length - 1; i >= 0 && price >= 0; i--) {
    count += Math.floor(price / coins[i])
    price = price % coins[i]
  }
  return count
}
