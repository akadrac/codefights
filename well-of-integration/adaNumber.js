function adaNumber(line) {
  line = line.replace(/_/g, '')
  if (line.length < 2)
    return false

  var last = line.lastIndexOf("#")
  var first = line.indexOf("#")

  if (first == -1) { // integer only
    return check(10, line)
  }

  if (line.endsWith("#") && last == line.length - 1 && first < line.length - 2) {
    var value = line.substring(first + 1, line.length - 1)
    var numerial = line.substring(0, first)
    if (numerial > 1 && numerial < 17) {
      return check(numerial, value)
    }
  }
  return false
}

function check(numerial, value) {
  for (var i = 0; i < value.length; i++) {
    var v = (parseInt(value[i], numerial))
    if (!Number.isInteger(v) || v >= numerial) {
      return false
    }
  }
  return true
}