function threeSplit(a) {
  var value = a.sum() / 3
  var sum1 = 0, sum2 = 0, sum3 = 0, ans = 0;
  for (var i = 0; i < a.length - 2; i++) {
    sum1 += a[i];
    if (sum1 == value) {
      sum2 = 0;
      for (var j = i + 1; j < a.length - 1; j++) {
        sum2 += a[j];
        if (sum2 == value) {
          sum3 = 0;
          for (var k = j + 1; k < a.length; k++) {
            sum3 += a[k];
          }
          if (sum3 == value) {
            ans++;
          }
        }
      }
    }
  }

  return ans;
}

Array.prototype.sum = function () {
  if (this.length === 0)
    return 0
  return this.reduce((previousValue, currentValue) => {
    return previousValue + currentValue
  })
}

// slower version below

function old(a) {
  var div = a.sum() / 3
  // console.log("sum", div)

  var first = walk(a, div)
  // console.log(first)
  var result = first.map(elm => {
    // var count = 0
    return (second = walk(elm[1], div)).length
    // console.log("e", second)
    // for (var i = 0; i < second.length; i++) {
    //   // console.log("s", second[i][0], second[i][1])
    //   if (second[i][1].test)
    //     count++
    // }
    // return second.length
  })

  return result.sum()
}

walk = (array, value) => {
  var a = []
  for (var x = array.length - 1; x > 0; x--) {
    var b = array.slice(0, x)
    if (b.test(value)) {
      a.push([b, array.slice(x)])
    }
  }
  return a
}

Array.prototype.test = function (value) {
  return (this.sum() === value) ? true : false
}
