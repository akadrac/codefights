function integerToStringOfFixedWidth(number, width) {
  var n = number.toString()
  var l = n.length
  var w = width
  var z = ""
  for (var i = 0; i < w - l; i++) {
    z += "0"
  }

  return (l == w) ? n : (w < l) ? n.substring(l - w) : z.concat(n)
}

