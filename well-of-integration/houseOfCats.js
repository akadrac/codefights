function houseOfCats(legs) {
  var people = []

  // go backwards doing max people, then take away a cat leg count and loop
  //     
  for (var i = legs; i >= 0; i -= 4) { // reduce legs count by a cats
    people.push(i / 2) // two legs for people
  }

  // sort into lowest to highest    
  return people.sort((a, b) => { return a - b })
}
