function arrayConversion(inputArray) {
    let count = 1
    while (inputArray.length > 1) {
        let result = []
        for (i = 0; i < inputArray.length; i++) {
            result.push((count % 2) ? inputArray[i] + inputArray[++i] : inputArray[i] * inputArray[++i])
        }
        inputArray = result
        count++
    } 
    return inputArray[0]
}