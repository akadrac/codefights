function squareDigitsSequence(a0) {
  var array = [];
  var count = 1;
  var result = a0;
  var str = a0.toString()[Symbol.iterator]();
  do {
    array.push(result)
    count++;
    result = 0;
    while (char = str.next().value) {
      result += Math.pow(char, 2);
    }
    str = result.toString()[Symbol.iterator]();
  } while (array.indexOf(result) == -1)
  return count;
}