function pagesNumberingWithInk(current, numberOfDigits) {
  current--;
  var printed = 0;
  do {
    current++;
    printed += current.toString().length;
    console.log("current %s, printed %s", current, printed)
  } while (printed < numberOfDigits)
  if (printed > numberOfDigits)
    current--;
  return current;
}
