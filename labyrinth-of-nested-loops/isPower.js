function isPower(n) {
  return power(n, 2);
}

function power(n, p) {
  console.log("number %s - power %s", n, p)
  var calc = 0
  var i = 0
  do {
    i++
    calc = Math.pow(i, p)
    console.log("i %s - p %s - calc %s", i, p, Math.abs(calc))
  }
  while (calc < n)
  return (calc == n) ? true : (p > Math.sqrt(n)) ? false : power(n, ++p);
}
