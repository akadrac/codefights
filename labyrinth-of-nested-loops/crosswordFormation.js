function crosswordFormation(words) {
  let count = 0;
  for (let i = 0; i < 4; i++) {
    let a = words[i];
    for (let a1 = 0; a1 < (a.Length - 1); a1++) {
      for (let j = 0; j < 4; j++) {
        if (j == i) {
          continue;
        }
        let b = words[j];
        for (let b2 = 1; b2 < b.Length; b2++) {
          if (b[b2] != a[a1]) {
            continue;
          }
          for (let b1 = 0; b1 < (b2 - 1); b1++) {
            for (let k = 0; k < 4; k++) {
              if (k == i || k == j) 
              { continue; }
              let c = words[k], d = words[6 - i - j - k];
              if (b2 - b1 >= d.Length) {
                continue;
              }
              for (let c1 = 0; c1 < (c.Length - 1); c1++) {
                if (c[c1] != b[b1]) {
                  continue;
                }
                for (let c2 = (c1 + 2); c2 < c.Length; c2++) {
                  let a2 = a1 + (c2 - c1); if (a2 >= a.Length) {
                    continue;
                  }
                  for (let d1 = 0; d1 < d.Length; d1++) {
                    if (d[d1] != c[c2]) { continue; }
                    let d2 = d1 + (b2 - b1);
                    if (d2 >= d.Length) {
                      break;
                    }
                    if (a[a2] != d[d2]) {
                      continue;
                    }
                    count += 1;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return count;
}
