function isSumOfConsecutive2(n) {
  var start = 0;
  var count = 0;
  var result = 0
  var inc = 0
  do {
    result = inc = ++start;
    // console.log("inc %s", inc)
    do {
      result += ++inc;
      // console.log(result);
    } while (result < n)
    if (result == n)
      count++;
  } while (start < Math.ceil(n / 2))
  return count;
}

