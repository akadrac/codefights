function comfortableNumbers(L, R) {
  return (L < R) ? a(L, R) : 0;
}

function a(l, r) {
  var count = 0

  for (var a = l; a < r; a++) {
    var sumA = sum(a);
    for (var b = a + 1; b <= r; b++) {
      var sumB = sum(b)
      if ((b >= a - sumA && b <= a + sumA) && (a >= b - sumB && a <= b + sumB)) {
        count++
      }
    }
  }
  return count
}

function sum(n) {
  var count = 0;
  var len = n.toString().length
  var sum = 0
  do {
    count++;
    sum += l = n % 10;
    n = (n - l) / 10;
  } while (len > count)
  return sum;
}
