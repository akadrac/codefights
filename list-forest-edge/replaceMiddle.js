function replaceMiddle(arr) {
  var odd = arr.length % 2;
  if (!odd) {
    var mid = arr.length / 2;
    var result = arr[mid - 1] + arr[mid];
    arr.splice(mid - 1, 2, result);
  }
  return arr;
}
