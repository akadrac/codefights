function makeArrayConsecutive2(sequence) {
  sequence.sort((a, b) => { return a - b });

  var start = sequence[0];
  var last = sequence[sequence.length - 1];

  return last - start - sequence.length + 1 | 0;
}
