function removeArrayPart(inputArray, l, r) {
  var array = []
  for (var i = 0; i < inputArray.length; i++) {
    if (i < l || i > r)
      array.push(inputArray[i])
  }
  return array;
}
