function knapsackLight(value1, weight1, value2, weight2, maxW) {
  var model = {
    hW: (value1 >= value2) ? weight1 : weight2,
    hV: (value1 >= value2) ? value1 : value2,
    lW: (value1 < value2) ? weight1 : weight2,
    lV: (value1 < value2) ? value1 : value2,
  }

  if (model.hW + model.lW <= maxW)
    return model.hV + model.lV;
  if (model.hW <= maxW)
    return model.hV;
  if (model.lW <= maxW)
    return model.lV;
  return 0;
}
