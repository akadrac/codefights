function arithmeticExpression(A, B, C) {
  function add() {
    return (A + B == C);
  }
  function sub() {
    return (A - B == C);
  }
  function mult() {
    return (A * B == C);
  }
  function div() {
    return (A / B == C);
  }
  return add() | sub() | mult() | div();
}
