function isInfiniteProcess(a, b) {
  if (a > b || b - a == 1 || (a + b) % 2 != 0)
    return true;
  return false;
}
