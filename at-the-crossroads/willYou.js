function willYou(young, beautiful, loved) {
  if (loved) {
    if (young && beautiful)
      return false;
    else
      return true;
  }
  if (!loved) {
    if (!beautiful || !young)
      return false;
    else
      return true;
  }
}
